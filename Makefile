.PHONY: default
default:
	black .
	flake8 .
	coverage run --source='.' manage.py test --noinput
	coverage report

.PHONY: migrations-and-migrate
migrations-and-migrate:
	 ./manage.py makemigrations
	 black -q .
	 ./manage.py migrate

