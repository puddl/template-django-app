This is based on https://gitlab.com/puddl/template-django-app


# TEMPLATE NOTICE
This is a template repo!

First, clone this to another place and change the remote
```
git clone https://gitlab.com/puddl/template-django-app.git fooapp
cd fooapp/
git remote rename origin template
git branch --unset-upstream
```

You may wish to replace the example names:
```
files=$(git ls-files)
sed -i -e 's/appname/fooapp/g' $files
sed -i -e 's/Appname/Fooapp/g' $files
git mv appname fooapp
```

Check the license and remove this notice from the README.
```
$EDITOR LICENSE
$EDITOR README.md
```


# Development
Create a virtualenv and install
```
pyenv virtualenv 3.9.5 appname
pyenv local appname
pip install -U pip

pip install -e .[dev]
```

Create a database
```
puddl db create appname
db_url=$(puddl db url appname)
# postgresql://appname:***@127.0.0.1:13370/puddl?application_name=appname
```

Configure and start
```
cat <<EOF > .env
SECRET_KEY=change-me
STATIC_ROOT=env/dev/static_root/
MEDIA_ROOT=env/dev/media_root/

DB_URL=${db_url}
DEBUG=true
EOF

pytest

./manage.py makemigrations
./manage.py migrate
./manage.py runserver
```

See the following for more environment variables:
```
grep os.environ appname/demosite/settings.py
```


# Dependencies and their reasons
- `dj-database-url` because it's by jacobian
- `django_extensions` for `./manage.py shell_plus --print-sql`
- `psycopg2-binary` because PostgreSQL is the best
- `python-dotenv` because `.env` is nice

To compile dependencies into a `requirements.txt` file, run
```
pip-compile
```
